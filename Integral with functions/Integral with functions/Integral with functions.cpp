﻿#include <iostream>
#include <string>
#include <fstream>
#include <math.h>

using namespace std;

double Trapeze(const double** const arr, const int count)
{
	double sum_trap = 0;
	for (int i = 0; i < count - 1; i++)
	{
		sum_trap += ((arr[i][1] + arr[i + 1][1]) / 2.0) * (arr[i + 1][0] - arr[i][0]);
	}
	return sum_trap;
	//cout << "sum_trap = " << sum_trap << endl;
}

double Rectangl(const double** const arr, const int count)
{
	double sum_rect = 0;
	for (int i = 0; i < count - 1; i++)
	{
		sum_rect += (arr[i][1] * (arr[i + 1][0] - arr[i][0]));

	}
	return sum_rect;
	//cout << "sum_rect = " << sum_rect << endl;
}

double Simpson1(const double* const arr[], const int count)
{
	double sum_s = 0;
	int p = ((count - 1) / 2);
	sum_s = ((arr[count - 1][0] - arr[0][0]) / 6.0) * (arr[0][1] + 4 * arr[p][1] + arr[count - 1][1]);

	return sum_s;
	//cout << "sum_s =" << sum_s << endl;
}


int main()
{
	string path = "pulse14_current.csv";

	//string path = "parabel.txt";

	ifstream charge;
	charge.open(path);
	if (!charge.is_open())
	{
		cout << "File is not found\n";
		cout << "exit";
		cin.get();
		return -1;
	}
	else
	{
		cout << "Opening file\n";
	}


	char buf[255u];

	int count = 0;
	do
	{
		charge.getline(buf, 255u);
		count++;
	} while (!charge.eof());

	if (buf[0] == '\0')
		count--;

	charge.clear();
	charge.seekg(std::ios_base::beg);

	double** arr = new double* [count];


	for (int i = 0 ; i < count; ++i)
	{
		arr[i] = new double[2u];
		charge >> arr[i][0] >> arr[i][1];
	}

	//Trapeze(arr, count);
	//Rectangl(arr, count);
	//Simpson1(arr, count);
	
	double a = Trapeze(const_cast<const double** const> (arr), count);
	cout << "Integral with trapeze = " << a << endl;

	double b = Rectangl(const_cast<const double** const> (arr), count);
	cout << "Integral with rectangle = " << b << endl;

	double c = Simpson1(const_cast<const double** const> (arr), count);
	cout << "Integral with Simpson = " << c << endl;

	delete[] arr;
	charge.close();
	return 0;
}
